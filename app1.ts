//acciones 
interface Action {
    type: string,
    payload?: any
}


function reducer(state = 10, action: Action) {


    switch (action.type) {
        case 'INCREMENTAR':
            return state += 1;
        case 'DECREMENTAR':
            return state -= 1;
        case 'MULTIPLICAR':
            return state * action.payload;
        case 'DIVIDIR':
            return state / action.payload;
        default:
            return state;
    }
}

// usar el reducer
const incrementadorAccion: Action = {
    type: 'INCREMENTAR'
}

console.log(reducer(10, incrementadorAccion));

const decrementaroAction: Action = {
    type: 'DECREMENTAR'
}

console.log(reducer(10, decrementaroAction));


const multiplicarAction: Action = {
    type: 'MULTIPLICAR',
    payload: 2
}

console.log(reducer(10, multiplicarAction));



const dividirAction: Action = {
    type: 'DIVIDIR',
    payload: 2
}

console.log(reducer(10, dividirAction));
