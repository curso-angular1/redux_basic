import { Store, createStore } from 'redux';
import { Contadoreducer } from './contador/contador.reducer';
import { incrementadorAccion } from './contador/contador.actions';

const store: Store = createStore(Contadoreducer);

store.subscribe(()=>{
    console.log('Subs:'+ store.getState());
})

store.dispatch(incrementadorAccion);
