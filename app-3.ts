import { Reducer, Action } from './ngrx-fake/ngrx';
import { Contadoreducer } from './contador/contador.reducer';
import { incrementadorAccion, multiplicarAction } from './contador/contador.actions';

class Store<T> {
    constructor(private reducer: Reducer<T>, private state: T) { }

    getState() {
        return this.state;
    }
    dispatch(action: Action) {
        this.state = this.reducer(this.state, action);
    }
}

const store = new Store(Contadoreducer, 10);

console.log(store.getState());

store.dispatch(incrementadorAccion);

console.log(store.getState());
store.dispatch(multiplicarAction);
console.log(store.getState());
